<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_m extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function all()
    {
        $this->db->select("*");
        $this->db->from("transaksi");
        $data = $this->db->get()->result();
        return $data;
    }
    public function add()
    {
        $tr = $this->cek_transaksi3();
        $tr2 = $this->cek_transaksi4()->num_rows();
        //$cek_transaksi = $this->cek_transaksi2($tgl);
        if($tr2 == 0){
        if($tr->num_rows() == 0){
            $id_transaksi = date('Ymd').'_1';
            $data = array(
                'id_transaksi' => $id_transaksi,
                'tr_date'=> date('Y-m-d'),
                'status'=> '1'
            );
            $this->db->insert('transaksi',$data);
        } else if($tr->num_rows() > 0){
            $data = $tr->row();
            $id_tr = $data->id_transaksi;
            $id_tr2 = substr($id_tr,0,8);
            if($id_tr2 == date('Ymd')){
                $no = str_replace($id_tr,$id_tr2.'_',"");
                $no2 = $no + 1;
                $id_tr3 = date('Ymd').'_'.$no2;
            } else{
                $id_tr3 = date('Ymd').'_1';
            }
            $data = array(
                'id_transaksi'=>$id_tr3,
                'tr_date'=>date('Y-m-d'),
                'status'=>'1'
            );
            $this->db->insert('transaksi',$data);
            
        }
        }
        $tr4 = $this->cek_transaksi3()->row();
        return $tr4;
    }
    public function insert($data)
    {
        $this->db->insert('transaksi',$data);
        return 1;
    }
    public function update($barang)
    {
        $this->db->where('kd_barang',$kd_barang);
        $this->db->update('transaksi',$barang);
        return 1;
    }
    public function delete($id)
    {
        $this->db->where('id_transaksi',$id);
        $this->db->delete('transaksi');
        return 1;
    }
    public function view($id)
    {
        
        $this->db->select("*");
        $this->db->from("barang");
        $this->db->where("id_transaksi",$id);
        $data = $this->db->get()->row();
        return $data;
    }
    public function cek_transaksi($tgl)
    {
        
        $this->db->select("*");
        $this->db->from("transaksi");
        $this->db->where('id_transaksi',$tgl."_1");
        $hasil = $this->db->get()->row();
        return $hasil;
        
    }
    public function cek_transaksi2($tgl)
    {
        
        $this->db->select("*");
        $this->db->from("transaksi");
        $this->db->like($tgl);
        $this->db->order_by('id_transaksi','desc');
        $hasil = $this->db->get()->row();
        return $hasil;
        
    }
    public function cek_transaksi3()
    {
        
        $this->db->select("*");
        $this->db->from("transaksi");
        $this->db->order_by('id_transaksi','desc');
        $hasil = $this->db->get();
        return $hasil;
        
    }
    public function cek_transaksi4()
    {
        $this->db->select("*");
        $this->db->from("transaksi");
        $this->db->where('status','1');
        return $this->db->get();
    }
    public function cek_query()
    {
        $hasil = $this->load->database();
        $hasil->where('status','1');
        return $hasil->get('transaksi')->row();
    }
    
}
