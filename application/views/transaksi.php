<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include 'header.php';
?><!DOCTYPE html>

<!DOCTYPE html>
<html lang="en">
<head>
</head>
<div class="modal" id="modal_barang">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Daftar Barang</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="container">
            <div class="row">
          <table class="table">
            <thead>
              
                <tr>
                <th>ID Barang</th>
                <th>Nama Barang</th>
                <th>Harga Barang</th>
                <th>Jumlah</th>
                </tr>
                
              </thead>
              <tbody>
                  <?php foreach($barang as $row){?>
                <tr>
                    <td contenteditable="true"><?= $row->kd_barang;?></td>
                    <td contenteditable="true"><?= $row->n_barang;?></td>
                    <td contenteditable="true"><?= $row->harga;?></td>
                    <td contenteditable="true">0</td>
                  </tr>
                  <?php }?>
              </tbody>
            </table>
          </div>
            <div class="row">
              <button class="btn btn-primary">Tambahkan</button>
              </div>
            </div>
          
      </div>

      <!-- Modal footer -->
      

    </div>
  </div>
</div>
<body id="page-top">


<div id="wrapper">
	<div id="content-wrapper">
		<div class="container-fluid">

        <!-- 
        karena ini halaman overview (home), kita matikan partial breadcrumb.
        Jika anda ingin mengampilkan breadcrumb di halaman overview,
        silahkan hilangkan komentar (//) di tag PHP di bawah.
        -->
		<?php //$this->load->view("admin/_partials/breadcrumb.php") ?>

		<!-- Icon Cards-->
		

		<!-- Area Chart Example-->
		<div class="card ">
            <div class="row">
                
			
			<div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                <span><b>No Faktur: <?php echo $no_transaksi?></b></span>
                
                </div>
                <div class="container">
                <div class="row">
                    <button class="btn btn-success" id="btn_add_barang">Tambah Barang</button>
                </div>
                </div>
                </div>                
			</div>
                    
            </div>
			
		</div>

		</div>
		<!-- /.container-fluid -->

		<!-- Sticky Footer -->


	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

    
</body>
</html>


        <!-- <div class="container-fluid"> -->

          <!-- Page Heading -->
          <!-- <h1 class="h3 mb-4 text-gray-800 font-weight-bold">Kasir Ku</h1><h6><P>(MODE PENGEMBANG)</P></h6>
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary mb-1">Pendapatan (Bulan)</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">IDR 40,000</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary mb-1">Transaksi Hari Ini</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">50 Transaksi</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-handshake fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

						<div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary mb-1">Jumlah Stok Barang</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">40 Stok</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-inbox fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
        <!-- </div> -->
<?php //include 'footer.php';
?>


	<!-- Hak Cipta TECHNO ICE DEVELOPER -->

